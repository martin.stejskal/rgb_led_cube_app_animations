# About
 * There are animations for
 [RGB LED cube application](https://gitlab.com/martin.stejskal/rgb_led_cube_app)

# Folder structure
 * `cube dimension`/`animation name.3da.gz`
 * `cube dimension`/`group of animations`/`animation name.3da.gz`
