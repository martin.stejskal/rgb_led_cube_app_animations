# Script that automatically search all Python sources and check them against
# PEP8 standard.

# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

cd "$this_dir/.."
pwd

flake8 . --exclude venv
