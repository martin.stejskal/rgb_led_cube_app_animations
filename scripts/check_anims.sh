#!/bin/bash
# Script which prepare environment, install dependencies under virtual
# environment and run application.
#
# Created:  2018.10.28
# Modified: 2018.10.28
# Author:   Martin Stejskal

# Get project directory, so everything can be done through absolute paths ->
# -> can cann this script from anywhere
project_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

# And move to project directory - everything will be easy. Well, at least it
# should be
cd "$project_dir"

# Default parameters
exec_app=1
dry_run=0

# Check if we want dry run
if [ ! -z $1 ] ; then
  if [ $1 == "--dry-run" ] ; then
    dry_run=1
  fi

  if [ $1 == "--just-prepare-env" ] ; then
    exec_app=0
  fi
fi

if [ ! -d venv ] ; then
  echo "Folder venv does not exist. Creating...."
  if [ $dry_run -eq 0 ] ; then
    virtualenv -p python3 venv &&
    # Activate environment
    source venv/bin/activate &&
    # And install packages
    pip install -r requirements.txt
  fi
else
  echo "Folder venv already exist. No need to recreate"
  if [ $dry_run -eq 0 ] ; then
    # Activate environment
    source venv/bin/activate
  fi
fi

if [ $dry_run -eq 0 ] ; then
  if [ $exec_app -eq 1 ] ; then
    # And run applicaton
    cd ./check_animations_integrity && python3 check_animations.py
  fi
fi

# And get back
cd "$user_dir"
