#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
==================================
Check integrity of all animations
==================================

``author`` Martin Stejskal

``Created`` 2018.10.28

``Modified`` 2018.10.29

"""
import glob

# Libraries for log events
import logging
import logging.config

from lib.download_src import DownloadSrcCls

# Logging configuration file
log_cfg_file = "lib/logging.cfg"

# Main function
if __name__ == "__main__":
    # Load configure file for logging
    logging.config.fileConfig(log_cfg_file, None, False)
    logger = logging.getLogger('root')

    # Download actual sources - check if compatible with latest version
    download_src = DownloadSrcCls()

    # Now try to import and initialize Load/Save class
    from lib.ca_bg_proc_load_save import ConsoleAPIBgLoadSaveCls

    load_anim = ConsoleAPIBgLoadSaveCls(
        logger=logging.getLogger("Load animation"))

    # Create animation list
    anim_list = []
    for filename in glob.iglob("../../**/*.3da.gz", recursive=True):
        logger.info("Found animation {}".format(filename))
        anim_list.append(filename)

    # Try to load animations
    for filename in anim_list:
        data, timer = load_anim.load_animation(filename=filename)

    logger.info("  All animations checked (^_^)")
