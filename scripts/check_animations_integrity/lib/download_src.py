#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Download latest sources
===================================

``author`` Martin Stejskal

``version`` 0.0.2
"""
import logging
import os.path
# Standard library for interacting with URL
import urllib.request


class DownloadSrcCls:
    def __init__(self):
        self.logger = logging.getLogger('Download sources')
        # Sources
        dwnl_lst = [
            [
                "https://gitlab.com/martin.stejskal/rgb_led_cube_app/raw/master/rgb_led_cube_app/lib/cube_LED.py", # noqa
                "lib/cube_LED.py"],
            [
                "https://gitlab.com/martin.stejskal/rgb_led_cube_app/raw/master/rgb_led_cube_app/lib/ca/ca_bg_proc_load_save.py", # noqa
                "lib/ca_bg_proc_load_save.py"],
            [
                "https://gitlab.com/martin.stejskal/rgb_led_cube_app/raw/master/rgb_led_cube_app/lib/common.py", # noqa
                "lib/common.py"],
        ]

        for item in dwnl_lst:
            self.download(url=item[0], dst=item[1])

    # /__init__()

    def download(self, url, dst):
        """Download source as defined file

        :param url: Link to source
        :type url: str

        :param dst: Destination file
        :type dst: str
        """
        # Check if file exists -> if exists do not re-download
        if (os.path.exists(dst)):
            self.logger.warning("File {} already exists! Skipping."
                                "".format(dst))
        else:
            self.logger.info("Downloading {}...".format(dst))
            urllib.request.urlretrieve(url, dst)
    # /download()
    # |DownloadSrcCls
# /DownloadSrcCls
